# Gitlab CI templates for Flywheel gears

These Flywheel gear CI templates will run a number of jobs:

1. `pre:env`: Extract relevant information from the manifest.json.
2.  `build`: Build the docker images for 1) the gear 2) the gear+dev dependencies.
3. `test`: Run lint and tests within the gear+dev docker image context.
4. `publish:docker` (upon tag): Publish to gear image to dockerhub.
5. `publish:poetry` (upon tag): Publish to PyPi is `PYPI="true"`.

## poetry-pre-commit.yml

CI template for Flywheel gears based on pre-commit hooks to run validation test.
More on pre-commit
[here](https://gitlab.com/flywheel-io/flywheel-apps/utils/pre-commit-hooks)

IMPORTANT: it requires that pre-commit be pip-installed in the docker test
image and that the `.pre-commit-config.yaml` is copied over in the test Docker image.
See [this repo](https://gitlab.com/flywheel-io/flywheel-apps/templates/poetry-cow-says) for
an example.

### Usage:

#### Include `poetry-pre-commit.yml` ci-template in `.gitlab-ci.yml` of your gear:

```yaml
include:
  - project: flywheel-io/flywheel-apps/utils/ci-templates
    ref: main
    file: poetry-pre-commit.yml
```

#### Configure

* PYPI: Whether or not to publish to PYPI on tags, default "false".
* DOCKER_HUB: Whether or not to publish to docker hub on tags, default "true".

A final configuration may look like this:

```yaml
include:
  - project: flywheel-io/flywheel-apps/utils/ci-templates
    ref: main
    file: poetry-pre-commit.yml

variables:
  PYPI: "false"
```

## poetry-pre-commit-no-base.yml

CI template for gears for which the main docker image cannot be build by CI
because the base image is private (e.g. IMBIO gears).

Same as `poetry-pre-commit` but:
* `pre-commit` will be run in a docker container defined by the
  Dockerfile located at `tests/Dockerfile`.
* does not publish docker image to dockerhub.

#### Include `poetry-pre-commit-no-base.yml` ci-template in `.gitlab-ci.yml` of your gear:

```yaml
include:
  - project: flywheel-io/flywheel-apps/utils/ci-templates
    ref: main
    file: poetry-pre-commit-no-base.yml

variables:
  PYPI: "false"
```

#### configure

No configuration option is supported.

## poetry.yml

CI template based on custom variables to enable/disable validation checks.

### Usage:

#### Include `poetry.yml` ci-template in `.gitlab-ci.yml` of your gear:

```yaml
include:
  - project: flywheel-io/flywheel-apps/utils/ci-templates
    ref: main
    file: poetry.yml
```

#### Configure:

__required__:

* PACKAGE_DIR: Name of directory for package, used for code coverage. This variable is
  required __UNLESS__ you explicitly set `PYTEST_COVERAGE: "false"`

__optional__:

* BLACK: Whether to format with black, default "true".
* ISORT: Whether to format with isort, default "true".
* PYPI: Whether to publish to PYPI on tags, default "true".
* PYTEST_COVERAGE: Whether to generate coverage report, default "true".
* TESTDIR: Directory containing tests, default "tests".

A final configuration may look like this:

```yaml
include:
  - project: flywheel-io/flywheel-apps/utils/ci-templates
    ref: main
    file: poetry.yml

variables:
  NAME: test-gear
  PACKAGE_DIR: my_test_gear/
  PYPI: "false"
```

# Contributing

Install pre-commit:

1. `pip install pre-commit`
2. `pre-commit install`

Every commit, pre-commit will run a yaml validator, end of file fixer, and trailing
whitespace fixer
